var Search = (function() {
  var input, output, value;

  function _updateOutput(event) {
    value = input.value;
    output.innerHTML = value;
  }

  function init(querySelectorInput, querySelectorOutput) {
    input = document.querySelector(querySelectorInput);
    output = document.querySelector(querySelectorOutput);
    input.addEventListener("keyup", _updateOutput);
  }

  return {
    execute: init
  };
})();

document.addEventListener("keypress", function(event) {
  Search.execute(".input", ".message");
});
